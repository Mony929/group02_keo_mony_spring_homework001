package com.example.demo;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private LocalDateTime dateTime;
    private String status;
    private String message;
    T customer;

    public CustomerResponse(String message, T customer,LocalDateTime dateTime, String status) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
